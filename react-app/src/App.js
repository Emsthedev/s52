// import logo from './logo.svg';
import { Fragment } from 'react';
import { Container } from 'react-bootstrap'
import './App.css';
//components
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
//import CourseCard from './components/CourseCard'
//pages
// import Courses from './pages/Courses';
// import Home from './pages/Home';
//import Register from './pages/Register';
import Login from './pages/Login'


function App() {
  return (
  <Fragment>
    <AppNavbar/>
    <Container>
      {/* <Home/>
      <Courses/> */}
    {/*   <Register/> */}
    <Login/>
    </Container>
   
  </Fragment>
  );
}

export default App;
